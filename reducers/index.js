import { combineReducers } from "redux";
import characters from "./sw_reducer";

export default combineReducers({
  characters
});
