import { REHYDRATE } from "redux-persist/constants";

import {
  FETCH_CHARACTERS_REQUEST,
  FETCH_CHARACTERS_SUCCESS,
  FETCH_CHARACTERS_FAILURE
} from "../actions/types";

const INITIAL_STATE = {
  loading: true,
  error: null,
  count: 0,
  data: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case REHYDRATE:
      return {
        ...state,
        loading: false,
        data: action.payload.characters.data,
        count: action.payload.characters.count
      };
    case FETCH_CHARACTERS_REQUEST:
      return {
        ...state,
        loading: action.payload.loading
      };
    case FETCH_CHARACTERS_FAILURE:
      return {
        ...state,
        loading: action.payload.loading,
        error: action.payload.error
      };
    case FETCH_CHARACTERS_SUCCESS:
      return {
        ...state,
        loading: action.payload.loading,
        data: action.payload.data.results,
        count: action.payload.data.count
      };
    default:
      return { ...state };
  }
}
