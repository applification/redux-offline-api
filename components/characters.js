import React, { Component } from "react";
import { Text, View } from "react-native";
import { connect } from "react-redux";

import * as actions from "../actions";

class Characters extends Component {
  componentWillMount() {
    this.props.fetchSWCharacters();
  }

  render() {
    if (this.props.loading) {
      return (
        <View>
          <Text>loading...</Text>
        </View>
      );
    }

    return (
      <View>
        <Text>There are {this.props.count} characters in Star Wars.</Text>
        <Text>{this.props.data[0].name} is the first character.</Text>
        <Text>Apparently he has {this.props.data[0].eye_color} eyes!</Text>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state.characters.data,
    loading: state.characters.loading,
    error: state.characters.error,
    count: state.characters.count
  };
}

export default connect(mapStateToProps, actions)(Characters);
