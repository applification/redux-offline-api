import axios from "axios";
import {
  FETCH_CHARACTERS_REQUEST,
  FETCH_CHARACTERS_SUCCESS,
  FETCH_CHARACTERS_FAILURE
} from "./types";

const rootURL = "http://swapi.co/api/people";

export const fetchSWCharacters = () => async dispatch => {
  dispatch(fetchCharactersRequest());
  try {
    let { data } = await axios.get(rootURL);
    dispatch(fetchCharactersSuccess(data));
  } catch (e) {
    dispatch(fetchCharactersFailure(e));
  }
};

export function fetchCharactersRequest() {
  const data = {
    loading: true
  };
  return {
    type: FETCH_CHARACTERS_REQUEST,
    payload: data
  };
}

export function fetchCharactersSuccess(characters) {
  const data = {
    loading: false,
    data: characters
  };
  return {
    type: FETCH_CHARACTERS_SUCCESS,
    payload: data
  };
}

export function fetchCharactersFailure(error) {
  const data = {
    loading: false,
    error
  };
  return {
    type: FETCH_CHARACTERS_FAILURE,
    payload: data
  };
}
