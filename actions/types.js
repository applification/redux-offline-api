export const FETCH_CHARACTERS_REQUEST = "fetch_characters_request";
export const FETCH_CHARACTERS_SUCCESS = "fetch_characters_success";
export const FETCH_CHARACTERS_FAILURE = "fetch_characters_failure";
