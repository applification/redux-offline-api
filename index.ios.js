/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { AppRegistry, StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";

import store from "./store";
import Characters from "./components/characters";

export default class reduxOffline extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Characters />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});

AppRegistry.registerComponent("reduxOffline", () => reduxOffline);
